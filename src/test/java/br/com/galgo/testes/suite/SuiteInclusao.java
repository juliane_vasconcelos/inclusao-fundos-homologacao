package br.com.galgo.testes.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;
import br.com.galgo.testes.recursos_comuns.teste.ExemploAula;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ ExemploAula.class })
public class SuiteInclusao {

}
